<?php

/**
* Implements hook_drush_command().
*/
function drush_remove_broken_term_references_drush_command() {

  $commands['rbtr'] = array(
    'callback' => 'drush_rbtr',
    'description' => 'Remove Broken Term References.',
    'aliases' => array('rbtr'),
    'options' => array(
      'field' => 'Specify field to remove term references for.',
    ),
    'examples' => array(
      'drush rbtr' => 'Print help.',
      'drush rbtr --field=field_term_reference' => 'Clean specific field.',
      'drush rbtr --field=all' => 'Clean all term reference fields.',
    ),
  );

  return $commands;

}

/**
* Drush command logic.
* drush_[COMMAND_NAME]().
*/
function drush_rbtr() {

  // get option passed through command line
  $fieldArgument = drush_get_option('field', 0);

  // Overkill sanitization to prevent accidental database modification more than security
  // If you can run drush commands you already can run arbitrary queries
  $fieldArgument = preg_replace('/[^a-zA-Z_0-9]/','',$fieldArgument);

  // Get list of term reference fields
  $allFieldInfo = field_info_field_map();
  $referenceFields = [];
  foreach($allFieldInfo as $fieldName => $fieldInfo) {
    if(isset($fieldInfo['type']) && $fieldInfo['type'] == 'taxonomy_term_reference') {
      $referenceFields[] = $fieldName;
    }
  }

  // Decide what fields to act on
  $fieldsToActOn = [];
  if($fieldArgument) {
    if(in_array($fieldArgument,$referenceFields)) {
      $fieldsToActOn[] = $fieldArgument;
    }elseif($fieldArgument == 'all'){
      $fieldsToActOn = $referenceFields;
    }else {
      drush_print('The field you entered does not exist or is not a term reference.');
      return;
    }
  }else {
    drush_print('Specify a field or all fields using "drush rbtr --field=all" or "drush rbtr --field=field_example".');
  }

  // Get all existing tids
  $allTids = array_keys(db_query('SELECT DISTINCT(tid) FROM {taxonomy_term_data};')->fetchAllKeyed());

  // loop over fields to act on
  foreach($fieldsToActOn as $fieldToActOn) {

    // Get references in this field
    $references = array_keys(db_query('SELECT DISTINCT('.$fieldToActOn.'_tid) FROM {field_data_'.$fieldToActOn.'};')->fetchAllKeyed());

    // Get references in revision table for this field
    $referenceRevisions = array_keys(db_query('SELECT DISTINCT('.$fieldToActOn.'_tid) FROM {field_revision_'.$fieldToActOn.'};')->fetchAllKeyed());

    // Combine and deduplicate list of tids from references and revisions
    $allReferences = array_unique(array_merge($references,$referenceRevisions));

    // Find the items in references that are not real terms
    $brokenReferences = array_diff($allReferences,$allTids);

    // Give an update
    if(!count($brokenReferences)){
      drush_print('No broken references for '.$fieldToActOn);
    }

    // Remove broken references
    if(count($brokenReferences)) {
      $numDeleted = db_delete('field_data_'.$fieldToActOn)->condition($fieldToActOn.'_tid',$brokenReferences,'IN')->execute();
      $numDeleted += db_delete('field_revision_'.$fieldToActOn)->condition($fieldToActOn.'_tid',$brokenReferences,'IN')->execute();
      // Notify user
      drush_print('Deleted '.$numDeleted.' references to '.count($brokenReferences).' deleted terms for '.$fieldToActOn.'.');
    }

  }

}